open Relude.Globals;

let wrapJsExn = fn =>
  try (fn() |> Result.ok) {
  | Js.Exn.Error(e) => Result.error(e)
  };

module Native = {
  module Buffer = {
    [@bs.val] [@bs.scope "Buffer"]
    external allocInt: (int, int) => Node.Buffer.t = "alloc";

    [@bs.send]
    external toString:
      (Node.Buffer.t, [@bs.string] [ | `ascii | `base64 | `hex | `utf8]) =>
      string =
      "toString";

    [@bs.val] [@bs.scope "Buffer"]
    external fromHex: (string, [@bs.string] [ | `hex]) => Node.Buffer.t =
      "from";

    let fromHex = string => fromHex(string, `hex);

    [@bs.val] [@bs.scope "Buffer"]
    external fromUtf8: (string, [@bs.string] [ | `utf8]) => Node.Buffer.t =
      "from";

    let fromUtf8 = string => fromUtf8(string, `utf8);

    [@bs.val] [@bs.scope "Buffer"]
    external concat: array(Node.Buffer.t) => Node.Buffer.t = "concat";

    [@bs.send]
    external slice: (Node.Buffer.t, int, int) => Node.Buffer.t = "slice";
  };

  module Random = {
    [@bs.module "crypto"]
    external randomBytes: int => Node.Buffer.t = "randomBytes";
  };

  module Hash = {
    type t;

    [@bs.module "crypto"]
    external create: ([@bs.string] [ | `sha1 | `sha256]) => t = "createHash";

    [@bs.send] external update: (t, Node.Buffer.t) => unit = "update";

    [@bs.send] external digest: t => Node.Buffer.t = "digest";
  };

  module Scrypt = {
    [@bs.module "crypto"]
    external scrypt:
      (
        Node.Buffer.t,
        Node.Buffer.t,
        (
          Js.Null_undefined.t(Js.Exn.t),
          Js.Null_undefined.t(Node.Buffer.t)
        ) =>
        unit
      ) =>
      unit =
      "scrypt";

    [@bs.module "crypto"]
    external scryptSync: (Node.Buffer.t, Node.Buffer.t, int) => Node.Buffer.t =
      "scryptSync";

    let createSync = (secret, salt, keylength) =>
      wrapJsExn(() => scryptSync(secret, salt, keylength));
  };

  module CipherIV = {
    type t;

    [@bs.module "crypto"]
    external create:
      (
        [@bs.string] [
          | [@bs.as "aes-192-cbc"] `aes192cbc
          | [@bs.as "aes-256-cbc"] `aes256cbc
        ],
        Node.Buffer.t,
        Node.Buffer.t
      ) =>
      t =
      "createCipheriv";

    let create = (algorithm, key, iv) =>
      wrapJsExn(() => create(algorithm, key, iv));

    [@bs.send] external update: (t, Node.Buffer.t) => Node.Buffer.t = "update";

    let update = (cipher, buffer) => wrapJsExn(() => update(cipher, buffer));

    [@bs.send] external complete: t => Node.Buffer.t = "final";

    let complete = cipher => wrapJsExn(() => complete(cipher));
  };

  module DecipherIV = {
    type t;

    [@bs.module "crypto"]
    external create:
      (
        [@bs.string] [
          | [@bs.as "aes-192-cbc"] `aes192cbc
          | [@bs.as "aes-256-cbc"] `aes256cbc
        ],
        Node.Buffer.t,
        Node.Buffer.t
      ) =>
      t =
      "createDecipheriv";

    let create = (algorithm, key, iv) =>
      wrapJsExn(() => create(algorithm, key, iv));

    [@bs.send] external update: (t, Node.Buffer.t) => Node.Buffer.t = "update";

    let update = (cipher, buffer) => wrapJsExn(() => update(cipher, buffer));

    [@bs.send] external complete: t => Node.Buffer.t = "final";

    let complete = cipher => wrapJsExn(() => complete(cipher));
  };
};

let parseInput = input =>
  switch (input) {
  | `buffer(input) => input
  | `hex(input) => Native.Buffer.fromHex(input)
  | `string(input) => Native.Buffer.fromUtf8(input)
  };

module Hash = {
  let make = (algorithm, input) => {
    let hash = Native.Hash.create(algorithm);

    Native.Hash.update(hash, input |> parseInput);

    Native.Hash.digest(hash);
  };
};

let makeKey = (algorithm, password, salt) =>
  switch (algorithm) {
  | `aes192cbc => Native.Scrypt.createSync(password, salt, 24)
  | `aes256cbc =>
    `buffer(Native.Buffer.concat([|salt, password|]))
    |> Hash.make(`sha256)
    |> Result.ok
  };

module CipherIV = {
  let encryptBuffer = (algorithm, password, salt, iv, secret) => {
    makeKey(algorithm, password, salt)
    |> Result.flatMap(key => Native.CipherIV.create(algorithm, key, iv))
    |> Result.flatMap(cipher =>
         Native.CipherIV.update(cipher, secret)
         |> Result.flatMap(buffer =>
              Native.CipherIV.complete(cipher)
              |> Result.map(buffer2 =>
                   Native.Buffer.concat([|buffer, buffer2|])
                 )
            )
       );
  };

  let encrypt = (algorithm, password, salt, iv, encrypted) => {
    let password = parseInput(password);
    let salt = parseInput(salt);
    let iv = parseInput(iv);
    let secret = parseInput(encrypted);

    encryptBuffer(algorithm, password, salt, iv, secret);
  };
};

module DecipherIV = {
  let decryptBuffer = (algorithm, password, salt, iv, encrypted) => {
    makeKey(algorithm, password, salt)
    |> Result.flatMap(key => Native.DecipherIV.create(algorithm, key, iv))
    |> Result.flatMap(cipher =>
         Native.DecipherIV.update(cipher, encrypted)
         |> Result.flatMap(buffer =>
              Native.DecipherIV.complete(cipher)
              |> Result.map(buffer2 =>
                   Native.Buffer.concat([|buffer, buffer2|])
                 )
            )
       );
  };

  let decrypt = (algorithm, password, salt, iv, encrypted) => {
    let password = parseInput(password);
    let salt = parseInput(salt);
    let iv = parseInput(iv);
    let encrypted = parseInput(encrypted);

    decryptBuffer(algorithm, password, salt, iv, encrypted);
  };
};

module Random = {
  let bytes = Native.Random.randomBytes;
};
