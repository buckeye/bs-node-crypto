open Relude.Globals;

let exnToString = e => {
  let name = Js.Exn.name(e) |> Option.getOrElse("<NO_NAME>");
  let filename = Js.Exn.fileName(e) |> Option.getOrElse("<NO_FILENAME>");
  let message = Js.Exn.message(e) |> Option.getOrElse("<NO_MESSAGE>");

  {j|$name :: $filename ::\n$message|j};
};

Ava.test(
  "Example: using the `cipher.update()` and `cipher.final()` methods", t => {
  let password =
    NodeCrypto.Native.Buffer.fromUtf8("Password used to generate key");
  let salt = NodeCrypto.Native.Buffer.fromUtf8("salt");
  let iv = NodeCrypto.Native.Buffer.allocInt(16, 0);
  let secret = NodeCrypto.Native.Buffer.fromUtf8("some clear text data");

  let expected = "e5f79c5915c02171eec6b212d5520d44480993d7d622a7c4c2da32f6efda0ffa";

  NodeCrypto.CipherIV.encryptBuffer(`aes192cbc, password, salt, iv, secret)
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> Result.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `hex))
  |> Result.map(string => Ava.Assert.is(t, string, expected))
  |> ignore;
});

Ava.test(
  "Example: using the `cipher.update()` and `cipher.final()` methods (string)",
  t => {
    let password = `string("Password used to generate key");
    let salt = `string("salt");
    let iv = `buffer(NodeCrypto.Native.Buffer.allocInt(16, 0));
    let secret = `string("some clear text data");

    let expected = "e5f79c5915c02171eec6b212d5520d44480993d7d622a7c4c2da32f6efda0ffa";

    NodeCrypto.CipherIV.encrypt(`aes192cbc, password, salt, iv, secret)
    |> Result.tapError(e =>
         Ava.Test.fail(~message=exnToString(e), t) |> ignore
       )
    |> Result.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `hex))
    |> Result.map(string => Ava.Assert.is(t, string, expected))
    |> ignore;
  },
);

Ava.test(
  "Example: using the `decipher.update()` and `decipher.final()` methods", t => {
  let password = `string("Password used to generate key");
  let salt = `string("salt");
  let iv = `buffer(NodeCrypto.Native.Buffer.allocInt(16, 0));

  let encrypted =
    `hex("e5f79c5915c02171eec6b212d5520d44480993d7d622a7c4c2da32f6efda0ffa");
  let expected = "some clear text data";

  NodeCrypto.DecipherIV.decrypt(`aes192cbc, password, salt, iv, encrypted)
  |> Result.tapError(e =>
       Ava.Test.fail(~message=exnToString(e), t) |> ignore
     )
  |> Result.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `utf8))
  |> Result.map(string => Ava.Assert.is(t, string, expected))
  |> ignore;
});

Ava.test(
  "https://hackernoon.com/managing-encryption-keys-with-aws-kms-in-node-js-c320c860019a",
  t => {
    let key = `string("This is from AWS"); // Plaintext key.
    let salt = `buffer(NodeCrypto.Random.bytes(16));
    let iv =
      `buffer(
        NodeCrypto.Native.Buffer.fromHex("00000000000000000000000000000000"),
      );
    let expected = "Shh... this is a secret";

    let secret = `string(expected);

    NodeCrypto.CipherIV.encrypt(`aes256cbc, key, salt, iv, secret)
    |> Result.flatMap(buffer =>
         NodeCrypto.DecipherIV.decrypt(
           `aes256cbc,
           key,
           salt,
           iv,
           `buffer(buffer),
         )
       )
    |> Result.tapError(e =>
         Ava.Test.fail(~message=exnToString(e), t) |> ignore
       )
    |> Result.map(buffer => NodeCrypto.Native.Buffer.toString(buffer, `utf8))
    |> Result.map(string => Ava.Assert.is(t, string, expected))
    |> ignore;
  },
);
